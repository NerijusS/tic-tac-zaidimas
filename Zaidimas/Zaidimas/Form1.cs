﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zaidimas
{
    public partial class Form1 : Form
    {
        bool turn = true;
        int turn_count = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("By Nerijus Skridaila", "Tic Tac Toe About");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (turn)
            {
                b.Text = "X";
            }
            else
            {
                b.Text = "0";
            }
            turn = !turn;
            b.Enabled = false;
            turn_count++;

            checkForWiner();
        }
        private void checkForWiner()
        {
            bool winer = false;


            if ((A1.Text == A2.Text) && (A2.Text == A3.Text) && (!A1.Enabled))
                winer = true;
            else if ((B1.Text == B2.Text) && (B2.Text == B3.Text) && (!B1.Enabled))
                winer = true;
            else if ((C1.Text == C2.Text) && (C2.Text == C3.Text) && (!C1.Enabled))
                winer = true;
            else if ((A1.Text == B1.Text) && (B1.Text == C1.Text) && (!A1.Enabled))
                winer = true;
            else if ((A2.Text == B2.Text) && (B2.Text == C2.Text) && (!A2.Enabled))
                winer = true;
            else if ((A3.Text == B3.Text) && (B3.Text == C3.Text) && (!A3.Enabled))
                winer = true;
            else if ((A1.Text == B2.Text) && (B2.Text == C3.Text) && (!A1.Enabled))
                winer = true;
            else if ((A3.Text == B2.Text) && (B2.Text == C1.Text) && (!C1.Enabled))
                winer = true;


            if (winer)
            {
                dissable_buttons();
                String winner = ""; // winner pavadinimas kitas nei bool winer !!!
                if (turn)
                {
                    winner = "0";
                    o_skaic.Text = (Int32.Parse(o_skaic.Text)+1).ToString();
                }
                else
                {
                    winner = "X";
                    x_skaic.Text = (Int32.Parse(x_skaic.Text) + 1).ToString();
                }
                MessageBox.Show(winner + " Laimejo");
            }
            else
            {
                if (turn_count == 9)
                {
                    lyg_skaic.Text = (Int32.Parse(lyg_skaic.Text) + 1).ToString();
                    MessageBox.Show("Lygiosios");
                }
            }
        }
        private void dissable_buttons()
        {

            foreach (Control c in Controls)
            {
                if (c is Button)
                {
                    Button b = (Button)c;
                    b.Enabled = false;
                }
            }
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            turn = true;
            turn_count = 0;

            foreach (Control c in Controls)
            {
                if (c is Button)
                {
                    Button b = (Button)c;
                    b.Enabled = true;
                    b.Text = "";
                }

            }
        }

        private void button_enter(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                if (turn)
                {
                    b.Text = "X";
                }
                else
                {
                    b.Text = "O";
                }
            }
        }

        private void button_leave(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b.Enabled)
            {
                b.Text = "";
            }
        }

        private void resetSkaiciavimaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            x_skaic.Text = "0";
            o_skaic.Text = "0";
            lyg_skaic.Text = "0";
        }
    }
}
